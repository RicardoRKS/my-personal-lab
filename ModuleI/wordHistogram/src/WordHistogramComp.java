import java.util.*;

public class WordHistogramComp implements Iterable<String> {

    private Map<String, Integer> map;

    public WordHistogramComp(String string) {
        this.map = new HashMap<>();
        String[] spliced = string.split("\\s+");
        for (String word: spliced) {
            map.put(word,keyValue(word));
        }
    }


    @Override
    public Iterator<String> iterator() {
        return map.keySet().iterator();
    }

    public int keyValue(String word){
        int key;
        if(map.containsKey(word)){
            key = map.get(word);
            key++;
        }
        else {
            key = 1;
        }
        return key;
    }


    public int size() {
        return map.size();
    }

    public int get(String word) {
     return map.get(word);
    }

    public Set<String> getKeySet(){
        return this.map.keySet();
    }
}
