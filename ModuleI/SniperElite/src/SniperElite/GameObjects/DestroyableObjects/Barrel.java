package SniperElite.GameObjects.DestroyableObjects;

import SniperElite.GameObjects.GameObject;

import java.util.Arrays;

public class Barrel extends GameObject implements Destroyable {

    private int currentDamage;
    private int maxDamage;
    private boolean destroyed;
    private BarrelType type;


    public Barrel() {
        this.currentDamage = 0;
        this.destroyed = false;
        this.type = BarrelType.chooseBarrel();
        this.maxDamage = type.getMaxDamage();
    }

    @Override
    public boolean isDestroyed() {
        return destroyed;
    }

    @Override
    public void hit(int damage) {
        if (currentDamage <= maxDamage) {
            currentDamage = currentDamage + damage;
        } else {
            destroyed = true;
        }
    }

/*    public BarrelType chooseBarrel() {
        options = BarrelType.values();
        int index = (int) Math.floor(Math.random() * options.length);
        return options[index];
    }*/

    @Override
    public String toString() {
        return "Barrel{" +
                "currentDamage=" + currentDamage +
                ", maxDamage=" + maxDamage +
                ", destroyed=" + destroyed +
                ", type=" + type +
                '}';
    }
}
