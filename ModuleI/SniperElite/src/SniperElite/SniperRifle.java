package SniperElite;

import SniperElite.GameObjects.DestroyableObjects.Destroyable;
import SniperElite.GameObjects.DestroyableObjects.Enemies.Enemy;

public class SniperRifle {

    private int bulletDamage;
    private int headshotCounter;
    private int missedShots;
    private int shotsFired;

    public SniperRifle(int bulletDamage) {
        this.bulletDamage = bulletDamage;
    }

    public int getBulletDamage() {
        return bulletDamage;
    }

    public void shoot(Destroyable unit) {
        double chance = Math.random();
        int damage = bulletDamage;
        double hitChance = 0.9d;
        /*double headshot = 0.05d;*/
        if (hitChance <= chance) {
            missedShots++;
            damage = 0;
        } /*else if (chance <= headshot) {
            headshotCounter++;
            damage = damage * 2;
            System.out.println("Headshot confirmed.");

        }*/
        unit.hit(damage);
        shotsFired++;
    }

    public int getHeadshotCounter() {
        return headshotCounter;
    }

    public int getShotsFired() {
        return shotsFired;
    }

    public int getMissedShots() {
        return missedShots;
    }

    public double getAccuracy() {
        double accuracy = Math.round((((double) shotsFired - missedShots) / shotsFired) * 100);
        return accuracy;
    }
}
