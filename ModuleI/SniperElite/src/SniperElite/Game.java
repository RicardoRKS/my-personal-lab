package SniperElite;

import SniperElite.GameObjects.DestroyableObjects.Barrel;
import SniperElite.GameObjects.DestroyableObjects.BarrelType;
import SniperElite.GameObjects.DestroyableObjects.Destroyable;
import SniperElite.GameObjects.DestroyableObjects.Enemies.ArmouredEnemy;
import SniperElite.GameObjects.DestroyableObjects.Enemies.Enemy;
import SniperElite.GameObjects.DestroyableObjects.Enemies.SoldierEnemy;
import SniperElite.GameObjects.GameObject;
import SniperElite.GameObjects.Tree;

import java.util.Arrays;

public class Game {

    private GameObject[] gameObjects;
    private SniperRifle sniperRifle;
    private int totalEnemies;
    private int health;
    private int armour;


    public Game(int bulletDamage, int totalEnemies, int health, int armour) {
        sniperRifle = new SniperRifle(bulletDamage);
        this.totalEnemies = totalEnemies;
        this.health = health;
        this.armour = armour;
        gameObjects = createObject(totalEnemies, health, armour);
    }

    public void start() {
        for (GameObject gameObject : gameObjects) {
            System.out.println("Searching for an enemy");
            if (gameObject instanceof Tree) {
                System.out.println("Just a tree... Calm down Ghost.");
            } else if (gameObject instanceof Barrel) {
                Barrel barrel = (Barrel) gameObject;
                System.out.println("Found a barrel, might as well just destroy it");
                while (!(barrel.isDestroyed())) {
                    sniperRifle.shoot((Destroyable) gameObject);
                    System.out.println("This barrel is tough...");
                    if (barrel.isDestroyed()) {
                        System.out.println("Barrel destroyed.");
                    }
                }
            } else {
                Enemy enemy = (Enemy) gameObject;
                System.out.println("Target confirmed.");
                while (!enemy.isDestroyed) {
                    sniperRifle.shoot(enemy);
                    if (enemy.isDestroyed) {
                        System.out.println("Kill confirmed.");
                    }
                }
            }
        }
        System.out.println("All enemies clear.");
        System.out.println("Shots Fired: " + sniperRifle.getShotsFired());
        //System.out.println("Headshots: " + sniperRifle.getHeadshotCounter());
        System.out.println("Missed shots " + sniperRifle.getMissedShots());
        System.out.println("Accuracy: " + sniperRifle.getAccuracy() + "%");

    }

    public GameObject[] createObject(int number, int health, int armour) {
        gameObjects = new GameObject[number];
        for (int i = 0; i < gameObjects.length; i++) {
            double chance = Math.random();
            if (chance > 0.9d) {
                gameObjects[i] = new Tree();
            } else if (chance > 0.8d) {
                gameObjects[i] = new Barrel();
                System.out.println(gameObjects[i]);
            } else if (chance > 0.3d) {
                gameObjects[i] = new SoldierEnemy(health);
            } else {
                gameObjects[i] = new ArmouredEnemy(health, armour);
            }
        }
        return gameObjects;
    }

    @Override
    public String toString() {
        return "Game{" +
                "gameObjects=" + Arrays.toString(gameObjects) +
                '}';
    }
}
