package rsimoes.player;

import rsimoes.player.grid.Grid;
import rsimoes.player.grid.Unit;
import org.academiadecodigo.simplegraphics.graphics.Color;

import static rsimoes.player.grid.Grid.CELL_SIZE;


public class MiddleMan extends Unit {

    public static final Color MIDDLEMANCOLOR = Color.YELLOW;
    public static final Color MAN_PAINTED_COLOR = Color.BLUE;

    public MiddleMan() {
        super(0, 0);
        rectangle.setColor(MIDDLEMANCOLOR);
        paint();
    }

    public enum Direction {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }

    public void moveToCoordinates(Direction direction) {
        switch (direction) {
            case UP:
                rectangle.translate(0, -CELL_SIZE);
                row--;
                break;
            case DOWN:
                rectangle.translate(0, CELL_SIZE);
                row++;
                break;
            case LEFT:
                rectangle.translate(-CELL_SIZE, 0);
                col--;
                break;
            case RIGHT:
                rectangle.translate(CELL_SIZE, 0);
                col++;
                break;
        }
    }
}

