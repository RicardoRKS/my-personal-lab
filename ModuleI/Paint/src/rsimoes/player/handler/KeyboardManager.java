package rsimoes.player.handler;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class KeyboardManager {
    public static void initKeyboard(KeyboardHandler kbHandler){
        Keyboard kb = new Keyboard(kbHandler);

        for (KeySet key: KeySet.values()) {
            KeyboardEvent event = new KeyboardEvent();
            event.setKeyboardEventType(key.TYPE);
            event.setKey(key.KEY_NUM);
            kb.addEventListener(event);
        }
    }
}
