package rsimoes.player;

import rsimoes.player.grid.Grid;
import rsimoes.player.grid.Unit;
import rsimoes.player.MiddleMan;

public class MapEditor {

    private Grid grid;
    private MiddleMan cursor;
    private boolean painting;

    public MapEditor(int rows, int cols) {
        grid = new Grid(rows, cols);
        cursor = new MiddleMan();
    }

    public void moveCursor(MiddleMan.Direction direction) {

        if (cursorOnEdge(direction)) {
            return;
        }

        switch (direction) {
            case UP:
            case RIGHT:
            case DOWN:
            case LEFT:
                cursor.moveToCoordinates(direction);
                break;
        }
        if (painting) {
            paintUnit();
        }
    }

    public void paintUnit() {
        Unit unit = grid.getCell(cursor.getRow(), cursor.getCol());
        if (unit.isPainted()) {
            unit.hide();
        } else {
            unit.paint();
        }
    }

    public void clear() {
        grid.clear();
    }

    public void load() {
        grid.load();
    }

    public void save() {
        grid.save();
    }

    public void setPainting(boolean painting) {
        this.painting = painting;
    }

    private boolean cursorOnEdge(MiddleMan.Direction direction) {
        return direction == MiddleMan.Direction.UP && cursor.getRow() == 0 ||
                direction == MiddleMan.Direction.DOWN && cursor.getRow() == grid.getRows() - 1 ||
                direction == MiddleMan.Direction.LEFT && cursor.getCol() == 0 ||
                direction == MiddleMan.Direction.RIGHT && cursor.getCol() == grid.getCols() - 1;
    }


}
