package org.academiadecodigo.bootcamp;

public class Game {

    private int secretNumber;
    private int maxNumber;
    private Player[] players;
    private boolean gameOver = false;

    public Game(int maxNumber, Player[] players) {

        this.players = players;
        this.maxNumber = maxNumber;
        secretNumber = NumberGenerator.randomNumber(maxNumber);
    }

    public void start() {

        System.out.println("The secret number for this game is " + secretNumber);

        while (!gameOver) {

            for (Player player : players) {

                if (player.guessNumber(maxNumber) == secretNumber) {
                    System.out.println(player.getName() + " is the winner!!!");
                    gameOver = true;
                    break;
                }
            }
        }
    }
}
