import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class WordReaderPart implements Iterable<String> {


    private final String fileName;


    public WordReaderPart(String file) {
        this.fileName = file;
    }

    @Override
    public Iterator<String> iterator() {
        return new WordReaderIterator();
    }

    private class WordReaderIterator implements Iterator<String> {

        private BufferedReader inBuffReader;

        private String[] words;
        private String currentLine;
        private int wordsIndex;

        public WordReaderIterator() {
            try {

                inBuffReader = new BufferedReader(new FileReader(fileName));

                currentLine = readLineOfText();
                words = getLineWords(currentLine);

            } catch (FileNotFoundException e) {
                throw new IllegalArgumentException(e);
            }
        }

        private String readLineOfText() {

            String line = null;

            try {
                line = inBuffReader.readLine();

                if (line == null) {
                    inBuffReader.close();
                    return null;
                }

                if (line.equals("") || line.matches("\\W+")) {
                    return readLineOfText();
                }
            } catch (IOException e) {
                e.printStackTrace();
                currentLine = null;
            }
            return line;
        }

        private String[] getLineWords(String line) {
            return line != null ? line.split("\\W+") : new String[0];
        }

        @Override
        public boolean hasNext() {
            return currentLine != null;
        }

        @Override
        public String next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            String result = words[wordsIndex];
            wordsIndex++;

            if (wordsIndex == words.length) {

                currentLine = readLineOfText();
                words = getLineWords(currentLine);
                wordsIndex = 0;
            }
            return result;
        }
    }
}


