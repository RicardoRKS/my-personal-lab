package org.academiadecodigo.floppybirds.fighters;

public abstract class Fighter {

    private String name;
    private int health;
    private int attackDamage;
    private int spellDamage;

    public Fighter(String name, FighterType type){
        this.name = name;
        this.attackDamage = type.ATTACK_DAMAGE;
        this.spellDamage = type.SPELL_DAMAGE;
        this.health = type.INITIAL_HEALTH;
    }

    public void hit(Fighter enemy){
        enemy.suffer(attackDamage);
    }

    public void cast(Fighter enemy){
        enemy.suffer(spellDamage);
    }

    public void suffer(int dmg){
        health -= dmg;
    }

    public boolean isDead(){
        return health <= 0;
    }

    @Override
    public String toString() {
        return "Fighter{" +
                "name='" + name + '\'' +
                "of the class " + this.getClass().getSimpleName() +
                ", health=" + health +
                ", attackDamage=" + attackDamage +
                ", spellDamage=" + spellDamage +
                '}';
    }
}
