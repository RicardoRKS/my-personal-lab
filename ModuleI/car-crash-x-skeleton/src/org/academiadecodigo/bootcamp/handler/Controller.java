package org.academiadecodigo.bootcamp.handler;

import org.academiadecodigo.bootcamp.car.UserCar;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;


public class Controller implements KeyboardHandler{


    private Keyboard keyboard;
    private UserCar mycar;

    public void init(UserCar mycar) {



        keyboard = new Keyboard(this);

        KeyboardEvent pressedRight = new KeyboardEvent();
        pressedRight.setKey(KeyboardEvent.KEY_RIGHT);
        pressedRight.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        keyboard.addEventListener(pressedRight);


        KeyboardEvent pressedLeft = new KeyboardEvent();
        pressedLeft.setKey(KeyboardEvent.KEY_LEFT);
        pressedLeft.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(pressedLeft);

        KeyboardEvent pressedUp = new KeyboardEvent();
        pressedUp.setKey(KeyboardEvent.KEY_UP);
        pressedUp.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(pressedUp);

        KeyboardEvent pressedDown = new KeyboardEvent();
        pressedDown.setKey(KeyboardEvent.KEY_DOWN);
        pressedDown.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(pressedDown);

    }


        @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

            switch (keyboardEvent.getKey()) {
                case KeyboardEvent.KEY_RIGHT:
                    mycar.moveRight();
                    break;
                case KeyboardEvent.KEY_LEFT:
                    mycar.moveLeft();
                    break;
                case KeyboardEvent.KEY_DOWN:
                    mycar.moveDown();
                    break;
                case KeyboardEvent.KEY_UP:
                    mycar.moveUp();
                    break;


            }
        }
            @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }
    public void setCar(UserCar car) {
        this.mycar = car;
    }


}
