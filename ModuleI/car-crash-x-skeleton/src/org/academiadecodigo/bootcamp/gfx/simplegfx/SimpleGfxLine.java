package org.academiadecodigo.bootcamp.gfx.simplegfx;

import org.academiadecodigo.bootcamp.grid.Grid;
import org.academiadecodigo.bootcamp.grid.GridColor;
import org.academiadecodigo.bootcamp.grid.position.AbstractGridPosition;
import org.academiadecodigo.bootcamp.grid.position.GridPosition;
import org.academiadecodigo.simplegraphics.graphics.Line;

public class SimpleGfxLine extends AbstractGridPosition {
    
    private Line line;
    private SimpleGfxGrid simpleGfxGrid;


    public SimpleGfxLine(int col, int row, SimpleGfxGrid grid) {
        super(col, row, grid);
        simpleGfxGrid = grid;
        int x = grid.columnToX(col);
        int y = grid.rowToY(row);
        line = new Line(x, y, grid.getCellSize(), grid.getCellSize());
        show();
    }


    @Override
        public void show() {
            line.draw();
        }

        /**
         * @see GridPosition#hide()
         */
        @Override
        public void hide() {
            line.delete();
        }

    public void setColor() {
        line.setColor(SimpleGfxColorMapper.getColor(GridColor.BLACK));
        super.setColor(GridColor.BLACK);
    }
}
