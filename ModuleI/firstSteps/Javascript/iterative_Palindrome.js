function iterativePalindrome(str){
	var current = 0;
// 	The for loop is limited by half of the string to be analysed. It then compares the first character with the last character. And after the i+1 caracter with the last-i caracter.
		for(current = 0; current <= Math.floor((str.length -1)/2); current++){
		var end = str.length - 1 - current;
			if(str[current] !== str[end]){
				return false;
			}
		}
		return true; // If the previous comparison is false, meaning the characters are not equal, the function will return true
}
var str = "malayalam";
console.log(iterativePalindrome(str));

