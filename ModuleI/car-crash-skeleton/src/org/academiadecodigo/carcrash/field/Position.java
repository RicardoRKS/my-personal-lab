package org.academiadecodigo.carcrash.field;

public class Position {

    private int col;
    private int row;

    public Position(int col, int row) {
        this.col = col;
        this.row = row;
    }

   public boolean isOutOfBounds(Direction direction){
       return Direction.UP == direction && row == 0
               || Direction.DOWN == direction && row == (Field.getHeight() - 1)
               || Direction.LEFT == direction && col == 0
               || Direction.RIGHT == direction && col == (Field.getWidth() - 1);
   }

    public boolean equals(Position position) {
        return col == position.getCol() && row == position.getRow();
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public void updatePosition(Direction direction) {
        switch (direction){
            case UP:
                row--;
                break;
            case DOWN:
                row++;
                break;
            case LEFT:
                col--;
                break;
            case RIGHT:
                col++;
                break;
        }
    }


    @Override
    public String toString() {
        return "Position{" +
                "col=" + col +
                ", row=" + row +
                '}';
    }
}
