package Hotel;

public class Client {
    private String guestName;
    private Hotel hotel;
    private boolean isGuest;

    public Client(String guestName, Hotel hotel) {
        this.guestName = guestName;
        this.hotel = hotel;
        isGuest = false;

    }

    public void checkIn(String guestName){
        int checkEmpty = hotel.checkEmpty();
        if(checkEmpty != -1 && !isGuest) {
            System.out.println("We have empty rooms!");
            System.out.println("You have been assigned to room number: " + hotel.setRoomFull(guestName) + ". Here's your key");
            isGuest = true;
        }
        else if (isGuest){
            System.out.println("You are already a guest at this hotel, should I bring you some water to help with the hangover?");
        }
        else {
            System.out.println("I'm sorry, we don't have any rooms available");

        }
    }
    public void checkOut(int roomNumber){
        hotel.setRoomEmpty(roomNumber);
        isGuest = false;
    }
    public void checkOut(String guestName){
        hotel.setRoomEmpty(guestName);
        isGuest = false;
    }
}

