package org.academiadecodigo.bootcamp.enums;

public enum Weapon {
    ROCK,
    PAPER,
    SCISSORS
}
