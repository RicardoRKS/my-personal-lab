package org.academiadecodigo.bootcamp;

import org.academiadecodigo.bootcamp.enums.Weapon;

public class GameEngine {

    private final Player player1, player2;

    public GameEngine(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    public String start() {

        player1.totalWins = 0;
        player2.totalWins = 0;
        String result = "";
        boolean gameOver = false;
        System.out.println("Game is about to start. Choose your weapon.");


        while (!gameOver) {

            player1.useWeapon();
            player2.useWeapon();
            Weapon player1Weapon = player1.useWeapon();
            Weapon player2Weapon = player2.useWeapon();
            String victoryPlayer1 = player1.getName() + " has won the round with " + player1Weapon + ". He now has " + player1.totalWins + " wins.";
            String victoryPlayer2 = player2.getName() + " has won the round with " + player2Weapon + ". He has now " + player2.totalWins + " wins.";
            String isDraw = "Its a draw! Both players picked " + player1Weapon + ". Go again!";


            if (player1.totalWins == 5) {
                gameOver = true;
                result = player1.getName() + " has won the final round with " + player1Weapon + ". His opponent chose " + player2Weapon;

            } else if (player2.totalWins == 5) {
                gameOver = true;
                result = player2.getName() + " has won the final round with " + player2Weapon + ". His opponent chose " + player1Weapon;

            } else if (player1Weapon == player2Weapon) {
                System.out.println(isDraw);

            } else if (player1Weapon == Weapon.ROCK && player2Weapon == Weapon.SCISSORS) {
                player1.totalWins = ++player1.totalWins;
                System.out.println(victoryPlayer1);
            } else if (player1Weapon == Weapon.PAPER && player2Weapon == Weapon.ROCK) {
                player1.totalWins = ++player1.totalWins;
                System.out.println(victoryPlayer1);
            } else if (player1Weapon == Weapon.SCISSORS && player2Weapon == Weapon.PAPER) {
                player1.totalWins = ++player1.totalWins;
                System.out.println(victoryPlayer1);

            } else {
                player2.totalWins = ++player2.totalWins;
                System.out.println(victoryPlayer2);

            }

        }
        return result;
    }

}
