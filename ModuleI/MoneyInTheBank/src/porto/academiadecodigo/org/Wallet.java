package porto.academiadecodigo.org;

public class Wallet {

    private float balance;

    public Wallet(float balance) {

        this.balance = balance;
    }

    public float getBalance() {
        return balance;
    }

    public String checkBalance() {
        return "Your wallet balance is " + balance;
    }

    public boolean operationPossible(float value) {
        if (value <= balance) {
            return true;
        } else {
            return false;
        }
    }

    public void depositBalance(float newBalance) {
        balance = balance + newBalance;
    }

    public void withdrawBalance(float newBalance) {
        balance = balance - newBalance;
    }
}
