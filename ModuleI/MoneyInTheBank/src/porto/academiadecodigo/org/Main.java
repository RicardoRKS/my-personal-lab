package porto.academiadecodigo.org;

public class Main {
    public static void main(String[] args) {
        Wallet ricardoWallet = new Wallet(0);
        Wallet sofiaWallet = new Wallet(0);
        Savings sharedSavings = new Savings(0);
        Person ricardo = new Person("Ricardo", ricardoWallet, sharedSavings);
        Person sofia = new Person("Sofia", sofiaWallet, sharedSavings);


        System.out.println("It's payday!");
        ricardo.toEarn(800);
        System.out.println("Time to blow up everything in games");
        ricardo.toSpend(300);
        System.out.println("Getting some money on my savings");
        ricardo.toDepositSavings(400);
        System.out.println("Now time for groceries");
        ricardo.toSpend(200);
        System.out.println("Oops, don't have cash, need to get some savings money");
        ricardo.toWithdrawSavings(100);
        ricardo.toSpend(200);
        System.out.println("Now lets check my final balance " + "\n" + ricardoWallet.checkBalance() + "\n" + sharedSavings.checkBalance());
        sofia.toEarn(800);
        sofia.toDepositSavings(200);
        sofia.toWithdrawSavings(600);
    }
}
