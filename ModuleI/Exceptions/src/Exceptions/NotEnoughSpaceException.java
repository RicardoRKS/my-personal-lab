package Exceptions;

public class NotEnoughSpaceException extends Exception {


    public NotEnoughSpaceException(){
        super("Not enough space");
    }
}
