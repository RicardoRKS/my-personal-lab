import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URL;

public class Main {
    public static void main(String[] args) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.readValue(new URL("https://api.github.com/users/ferrao"), Test.class);
    }
}
