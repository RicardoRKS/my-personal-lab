async function fetchGit(){

    var name = ${"#name"}.text();
    var data = fetch('https://api.github.com/search/users?q='+name+'+in:login').then(
      successResponse => {
        if (successResponse.status != 200) {
          return null;
        } else {
          return successResponse.json();
        }
      },
      failResponse => {
        return null;
      }
    );

    var results = await Promise.all(data);

    console.log(results);
}