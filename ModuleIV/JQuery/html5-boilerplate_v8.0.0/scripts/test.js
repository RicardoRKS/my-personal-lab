$(document).ready(function() {
    getCustomers();
});
function getCustomers() {
    $.ajax({
        url: "http://localhost:8080/javabank5/api/customer",
        async: true,
        success: successCallback,
        error: errorCallback
    });
}
function successCallback(response) {
    populateTable(response);
}
function errorCallback(request, status, error){

}

var populateTable = function(customerData) {
    customerData.forEach(customer => {
        var htmlStr =
            "<tr class=\"table\">" +
            "   <td>" + customer.id< + "</td>" +
            "   <td>" + customer.firstName + "</td>" +
            "   <td>" + customer.lastName + "</td>" +
            "   <td>" + customer.email + "</td>" +
            "   <td>" + customer.phone + "</td>" +
            "   <td> <button id=\"button_edit\" class=\"button_edit\" type=\"button\"> Edit </button> </td>" +
            "   <td> <button id=\"button_delete\" class=\"button_delete\" type=\"button\"> Delete </button> </td>" +
            "</tr>".appendTo('#users-table');
    });
}
