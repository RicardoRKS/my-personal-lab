/**
 * Return the result of invoking the provided function
 * If an exception is thrown, return the enclosed error message
 */
exports.callIt = function(fn) {
  try {
      return fn();
  } catch (Error){
      return Error.message;
    }
};

/**
 * Return true if the provided arguments are equal,
 * throw an error with an enclosed message otherwise
 */
exports.assertEqual = function(a, b) {
    if(a !== b){
        throw new Error("Error");
    }
    return true;
};

/**
 * Return a custom error constructor with a timestamp property
 * indicating when the error occurred
 */
exports.createCustomError = function() {
    var customError = function (message){
        var error = Error(message);
        this.name = "CustomError";
        this.message = error.message;
        this.timestamp = Date.now();
        this.stack = error.stack;
    }
    customError.prototype = Object.create(Error.prototype);
    customError.prototype.constructor = customError;
    return customError;
};
