/**
 * Return a stop watch object with the following API:
 * getTime - return number of seconds elapsed
 * start - start counting time
 * stop - stop counting time
 * reset - sets seconds elapsed to zero
 */
exports.createStopWatch = function() {
    var time = 0;
    var interval;
    var incrementTimer = function () {
        time++;
    }
    return {
        getTime: function () {
            return time;
        },
        start: function () {
            interval = setInterval(incrementTimer,1000);
        },
        stop: function () {
            clearInterval(interval);
        },
        reset: function () {
            time = 0;
        }
    }
};
