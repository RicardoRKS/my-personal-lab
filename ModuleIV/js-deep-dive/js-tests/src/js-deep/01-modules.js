/**
 * Creates a counter module with an initial value, zero if not provided
 */
exports.createCounter = function(counter) {
    var internalCounter = (typeof counter === "undefined") ? 0 : counter;
    return {
        get: function () {
            return internalCounter;
        },
        increment: function () {
            internalCounter++;
        },
        reset: function () {
            internalCounter = 0;
        }
    }
};

/**
 * Creates a person module with name and age
 * An initial name value should be provided and
 * an exception thrown if not
 */
exports.createPerson = function(name) {
    if(!name){
        throw new Error("No name!")
    }

    var personName = name;
    var personAge = 0;

    return {
        getName: function (){
            return personName;
        },
        getAge: function (){
            return personAge;
        },
        setName: function (name){
            personName = name;
        },
        setAge: function (age){
            personAge = age;
        }
    };
};
