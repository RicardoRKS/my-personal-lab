/**
 * Determine the location of an item in the array
 */
exports.indexOf = function(arr, item) {
    return arr.indexOf(item);
};

/**
 * Sum the items of an array
 */
exports.sum = function(arr) {
  return arr.reduce((x,y) => x + y,0)
};

/**
 * Remove all instances of an item from an array
 */
exports.remove = function(arr, item) {
    return arr.filter((x) => x !== item)
};

/**
 * Remove all instances of an item from an array by mutating original array
 */
exports.removeWithoutCopy = function(arr, item) {
    
    for(var i = arr.length; i > 0; i--){
        if (arr[i] === item) {
            arr.splice(arr.indexOf(item), 1);
        }
    }
return arr;
};

/**
 * Add an item to the end of the array
 */
exports.append = function(arr, item) {
    arr.push(item);
    return arr;
};

/**
 * Remove the last item of an array
 */
exports.truncate = function(arr) {
    arr.pop();
    return arr;
};

/**
 * Add an item to the beginning of an array
 */
exports.prepend = function(arr, item) {
    arr.unshift(item);
    return arr;
};

/**
 * Remove the first item of an array
 */
exports.curtail = function(arr) {
    arr.shift();
    return arr;
};

/**
 * Join two arrays together
 */
exports.concat = function(arr1, arr2) {
    return arr1.concat(arr2);
};

/**
 * Add an item to an array in the specified position
 */
exports.insert = function(arr, item, index) {
    arr.splice(index, 0, item);
    return arr;
};

/**
 * Count the number of occurrences of an item in an array
 */
exports.count = function(arr, item) {
   
    var count = function (acc, element) {
        if (element === item) {
            acc++;
        }
        return acc;
    };
    return arr.reduce(count, 0);
};

/**
 * Find all items which container multiple occurrences in the array
 */
exports.duplicates = function(arr) {

    var recursiveOccurrence = function (currentValue, _index, array) {
        return exports.count(array, currentValue) > 1;
    };

    var notDuplicate = function(currentValue, index, array) {
        return (array.indexOf(currentValue) === index);
    };

    return arr.filter(recursiveOccurrence).filter(notDuplicate);
};

/**
 * Square each number in the array
 */
exports.square = function(arr) {

   var square = function(value){
       return Math.pow(value,2);
   };

   return arr.map(square, arr);
};

/**
 * Find all occurrences of an item in an array
 */
exports.findAllOccurrences = function(arr, target) {

    var equals = function(element, index) {
        if( element === target){
            return index
        }
    };
    var notUndefined = function(element){
        return element !== undefined;
    };

    return arr.map(equals, arr).filter(notUndefined);
};
