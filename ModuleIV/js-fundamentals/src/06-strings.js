/**
 * Reduce duplicate characters to a desired minimum
 */
exports.reduceString = function(str, amount) {
    var regEx = new RegExp("(.)(?=\\1{" + amount + "})","g");
    return str.replace(regEx, "");
};

/**
 * Wrap lines at a given number of columns without breaking words
 */
exports.wordWrap = function(str, cols) {
    var toReturn = ('');
    var count = 0;
    for(let i = 0; i < str.length; i++){
        if(str[i] === " " && count >= cols){
            toReturn += "\n";
            count = 0; 
        }
        else{
            toReturn += str[i];
            count++;
        }
    }
    return toReturn;

};

/**
 * Reverse a String
 */
exports.reverseString = function(str) {
    return str.split('').reverse().join('');
};

/**
 * Check if String is a palindrome
 */
exports.palindrome = function(str) {
    return str == str.split('').reverse().join('');
};
