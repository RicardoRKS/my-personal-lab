package Manager;

import Handler.Messages;
import User.User;
import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.PasswordInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;

import static Manager.CredentialsManager.credentialsManager;

public class UI {

    private final Prompt prompt;
    private String userName;
    private String password;
    private User user;

    public UI(){
        prompt = new Prompt(System.in, System.out);
        mainMenu();
    }

    public void mainMenu(){
        String[] menuOptions = {"Login", "Register"};
        MenuInputScanner scanner = new MenuInputScanner(menuOptions);
        scanner.setMessage(Messages.WELCOME);
        scanner.setError(Messages.INVALID_OPTION);

        int answerIndex = prompt.getUserInput(scanner);
        if(answerIndex == 1){
            askCredentials();
            return;
        }
        registerAccount();
    }

    private void registerAccount() {
        System.out.println(Messages.REGISTER_ACCOUNT);

        askUserInput();

        boolean wrongPassword = true;

        while (wrongPassword) {
            PasswordInputScanner confirmPassword = new PasswordInputScanner();
            confirmPassword.setMessage(Messages.CONFIRM_PASSWORD);
            if (password.equals(prompt.getUserInput(confirmPassword))) {
                wrongPassword = false;
            }
            else {
                System.out.println(Messages.PASSWORDS_DO_NOT_MATCH);
            }
        }

        while (!credentialsManager.storeCredentials(userName, password)){
            System.out.println(Messages.USERNAME_ALREADY_EXISTS);
            askUserInput();
        }
        System.out.println(Messages.REGISTER_SUCCESSFUL);
        mainMenu();
    }


    public void askCredentials(){

        askUserInput();

        if(!(((user = credentialsManager.authenticate(userName,password))) == null)){
            System.out.println(Messages.LOGIN_SUCCESSFUL + userName);
            user.display();

        }
        else{
            System.out.println(Messages.INVALID_USERNAME_PASSWORD);
        }
    }

    public void askUserInput(){
        StringInputScanner usernameQuestion = new StringInputScanner();
        usernameQuestion.setMessage("Username: ");
        userName = prompt.getUserInput(usernameQuestion);

        PasswordInputScanner userPassword = new PasswordInputScanner();
        userPassword.setMessage("Password: ");
        password = prompt.getUserInput(userPassword);
    }
}
