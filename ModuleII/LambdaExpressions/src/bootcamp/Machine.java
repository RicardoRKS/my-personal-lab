package bootcamp;

public class Machine<T> {

    MonoOperation<Integer> integerMonoOperation = (i1) ->  i1*i1;

    MonoOperation<String> stringMonoOperation = String::toLowerCase;

    BiOperation<Integer> integerBiOperation = Integer::sum;

    BiOperation<String> stringBiOperation = (i1,i2) -> i1+i2;
    

}
