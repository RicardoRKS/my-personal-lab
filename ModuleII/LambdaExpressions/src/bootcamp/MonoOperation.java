package bootcamp;

public interface MonoOperation<T> {

    T singleOperation(T data);
}
