package Server.Commands;


import Client.TCPClient;
import Server.ServerWorker;
import Server.TCPServer;

public class ListHandler implements CommandHandler {
    @Override
    public void handle(TCPServer server, ServerWorker sender, String message) {
        sender.sendToClient(server.listClients());
    }
}

