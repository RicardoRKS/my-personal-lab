public enum StatusCode {

    OK(200),
    NOT_FOUND(404);

    private int code;

    StatusCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        switch (this) {
            case OK:
                return "OK";
            case NOT_FOUND:
                return "Not Found";
            default:
                return "Returning default";
        }
    }
}
