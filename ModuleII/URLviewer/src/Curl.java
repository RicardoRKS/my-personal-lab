import java.io.*;
import java.net.URL;

public class Curl {

    public static void main(String[] args) {
        Curl curl = new Curl();

        try {
            curl.askURL();
            curl.getURL();
            curl.getContents();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    private BufferedReader urlReader;
    private String urlName;
    public final String fileName = "src/resources/saveFile.txt";
    private BufferedWriter writer = null;

    public void askURL() throws IOException {
        BufferedReader userReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("URL:");
        urlName = userReader.readLine();
        if(!urlName.contains("https://")){
            urlName = "https://" + urlName;
        }
        userReader.close();
    }

    public void getURL() throws IOException {
        URL url = new URL(urlName);
       urlReader = new BufferedReader(new InputStreamReader(url.openStream()));
    }

    public void getContents() throws IOException {
        initReaders();
        String content;
        while((content = urlReader.readLine()) != null){
            System.out.println(content + "\n");
            writer.write(content);
            }
        urlReader.close();
        writer.close();
    }

    public void initReaders() throws IOException {
        writer = new BufferedWriter(new FileWriter(fileName));
    }
}
